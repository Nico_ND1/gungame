package de.meinaxia.gungame.setup.map;
import net.meinaxia.gungame.map.Map;
import net.meinaxia.gungame.map.MapManager;

/**
 * @author Nico_ND1
 */
public class EmptyMapManager implements MapManager {
    @Override
    public Map getCurrentMap() {
        return null;
    }

    @Override
    public void setCurrentMap(Map newMap) {

    }
}
