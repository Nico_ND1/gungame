package de.meinaxia.gungame.setup.command;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.level.Level;
import net.meinaxia.gungame.level.LevelItem;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class SetupCommand implements CommandExecutor {

    private final GunGame gunGame;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        /*
        setup level <Level> - (Inventar wird übernommen)
        setup clear <Level>
        setup maxlevel <Level>
         */

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("level")) {
                final int level = Integer.valueOf(args[1]);
                final List<LevelItem> items = new ArrayList<>();
                this.addItems(items, player.getInventory().getContents(), false);
                this.addItems(items, player.getInventory().getArmorContents(), true);

                this.gunGame.getLevelConfig().getLevels().removeIf(levelObject -> levelObject.getLevel() == level);
                this.gunGame.getLevelConfig().getLevels().add(new Level(level, items));
                player.sendMessage("Level hinzugefügt oder abgeändert.");
                return true;
            } else if (args[0].equalsIgnoreCase("clear")) {
                final int level = Integer.valueOf(args[1]);

                this.gunGame.getLevelConfig().getLevels().removeIf(levelObject -> levelObject.getLevel() == level);
                player.sendMessage("Level gelöscht.");
            } else if (args[0].equalsIgnoreCase("maxlevel")) {
                final int level = Integer.valueOf(args[1]);

                this.gunGame.getLevelConfig().setMaxLevels(level);
                player.sendMessage("Maximales Level gesetzt.");
            }
        }

        player.sendMessage("Benutzung: /" + label + " <level, clear, maxlevel> <Level>");

        return false;
    }

    private void addItems(List<LevelItem> items, ItemStack[] itemStacks, boolean armor) {
        for (int slot = 0; slot < itemStacks.length; slot++) {
            final ItemStack itemStack = itemStacks[slot];
            if (itemStack == null || itemStack.getType() == Material.AIR) continue;
            items.add(new LevelItem(slot + (armor ? 100 : 0), itemStack));
        }
    }

}
