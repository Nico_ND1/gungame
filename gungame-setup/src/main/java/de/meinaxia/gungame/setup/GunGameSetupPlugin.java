package de.meinaxia.gungame.setup;
import de.meinaxia.gungame.setup.command.SetupCommand;
import de.meinaxia.gungame.setup.map.EmptyMapManager;
import de.meinaxia.gungame.stats.StatsConfig;
import de.meinaxia.gungame.stats.mysql.StatsDatabaseMongoDB;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.level.LevelConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.FileNotFoundException;

/**
 * @author Nico_ND1
 */
public class GunGameSetupPlugin extends JavaPlugin {

    private GunGame gunGame;

    @Override
    public void onEnable() {
        try {
            this.gunGame = new GunGame(
                LevelConfig.loadConfig("plugins/GunGame/levels.json"),
                new EmptyMapManager(),
                new StatsDatabaseMongoDB(StatsConfig.loadConfig("plugins/GunGame/database.json"))
            );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.getCommand("setup").setExecutor(new SetupCommand(this.gunGame));
    }

    @Override
    public void onDisable() {
        this.gunGame.getLevelConfig().save("plugins/GunGame/levels.json");
    }
}
