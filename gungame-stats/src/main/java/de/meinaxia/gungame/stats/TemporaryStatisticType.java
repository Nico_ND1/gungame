package de.meinaxia.gungame.stats;
import lombok.AllArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public enum TemporaryStatisticType {

    ALL("-"),
    YEARLY("yyyy"),
    MONTHLY("MM.yyyy"),
    WEEKLY("yyyy w"),
    DAILY("yyyy DD"),
    HALF_DAY("yyyy DD.a");

    private final String datePattern;

    public String format() {
        return this.format(new Date());
    }

    public String format(long time) {
        return this.format(new Date(time));
    }

    public String format(Date date) {
        final DateFormat dateFormat = new SimpleDateFormat(this.datePattern);
        return this.name() + "-" + dateFormat.format(date);
    }

}
