package de.meinaxia.gungame.stats;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class StatsConfig implements JsonDeserializer<StatsConfig> {

    private static final Gson GSON = new GsonBuilder()
        .registerTypeAdapter(StatsConfig.class, new StatsConfig())
        .setPrettyPrinting()
        .serializeNulls()
        .create();

    public static StatsConfig loadConfig(String path) throws FileNotFoundException {
        return StatsConfig.GSON.fromJson(new InputStreamReader(new FileInputStream(mkdirPath(path)), Charset.forName("UTF-8")), StatsConfig.class);
    }

    private static File mkdirPath(String path) {
        final File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new StatsConfig(Collections.singletonList(TemporaryStatisticType.ALL), "host", 3306, "user", "password", "database").save(path);
        }
        return file;
    }

    @Expose private final List<TemporaryStatisticType> temporaryStatisticTypes;
    @Expose private final String host;
    @Expose private final int port;
    @Expose private final String user;
    @Expose private final String password;
    @Expose private final String database;

    @Override
    public StatsConfig deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final List<TemporaryStatisticType> types = new ArrayList<>();
        jsonObject.getAsJsonArray("temporaryStatisticTypes").forEach(jsonElement -> {
            types.add(TemporaryStatisticType.valueOf(jsonElement.getAsString()));
        });

        return new StatsConfig(
            types,
            jsonObject.get("host").getAsString(),
            jsonObject.get("port").getAsInt(),
            jsonObject.get("user").getAsString(),
            jsonObject.get("password").getAsString(),
            jsonObject.get("database").getAsString()
        );
    }

    public void save(String path) {
        final File file = StatsConfig.mkdirPath(path);

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(StatsConfig.GSON.toJson(this, StatsConfig.class));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
