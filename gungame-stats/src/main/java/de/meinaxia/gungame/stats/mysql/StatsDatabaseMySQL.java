package de.meinaxia.gungame.stats.mysql;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import de.meinaxia.gungame.stats.StatisticPlayer;
import de.meinaxia.gungame.stats.StatisticType;
import de.meinaxia.gungame.stats.StatsConfig;
import de.meinaxia.gungame.stats.StatsDatabase;
import de.meinaxia.gungame.stats.TemporaryStatisticType;
import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class StatsDatabaseMySQL implements StatsDatabase {

    private static final String TABLE_NAME = "gungamestats";

    private final BasicDataSource dataSource = new BasicDataSource();
    private final StatsConfig statsConfig;
    private final Cache<UUID, StatisticPlayer> playerCache = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build();

    public StatsDatabaseMySQL(StatsConfig config) {
        this.statsConfig = config;
        this.dataSource.setUrl("jdbc:mysql://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase());
        this.dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        this.dataSource.setUsername(config.getUser());
        this.dataSource.setPassword(config.getPassword());

        try {
            final Statement statement = this.dataSource.getConnection().createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (uuid VARCHAR(36) UNIQUE, temporarytype VARCHAR(100), " + this.formatType(false, false, true) + ");");
            statement.executeUpdate("CREATE UNIQUE INDEX `temporarytype` ON `" + TABLE_NAME + "` (`temporarytype`, `uuid`)");
            statement.close();
        } catch (SQLException e) {
        }
    }

    @Override
    public StatisticPlayer loadPlayer(UUID uuid) {
        return this.getPlayer(uuid, TemporaryStatisticType.ALL);
    }

    @Override
    public StatisticPlayer getPlayer(UUID uuid, TemporaryStatisticType temporaryStatisticType) {
        try {
            final Connection connection = this.dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE uuid=? AND temporarytype=?;");
            statement.setString(1, uuid.toString());
            statement.setString(2, temporaryStatisticType.format());
            final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return new StatisticPlayer(uuid, new HashMap<>());

            final Map<StatisticType, Integer> stats = new HashMap<>();
            for (StatisticType statisticType : StatisticType.values()) {
                stats.put(statisticType, resultSet.getInt(statisticType.name().toLowerCase()));
            }
            statement.close();
            //connection.close();

            final StatisticPlayer statisticPlayer = new StatisticPlayer(uuid, stats);
            this.playerCache.put(uuid, statisticPlayer);
            return statisticPlayer;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new StatisticPlayer(uuid, new HashMap<>());
    }

    @Override
    public void saveStats(StatisticPlayer statisticPlayer) {
        this.saveStats(statisticPlayer.getUuid(), statisticPlayer.getDifference());
        statisticPlayer.getDifference().clear();
        this.playerCache.invalidate(statisticPlayer.getUuid());
    }

    @Override
    public StatisticPlayer getPlayer(UUID uuid) {
        final StatisticPlayer statisticPlayer = this.playerCache.getIfPresent(uuid);
        if (statisticPlayer == null) {
            return this.loadPlayer(uuid);
        }
        return statisticPlayer;
    }

    @Override
    public List<StatisticPlayer> getTopList(StatisticType statisticType, TemporaryStatisticType temporaryStatisticType, int limit) {
        try {
            final Connection connection = this.dataSource.getConnection();
            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE temporarytype=? ORDER BY " + statisticType.name().toLowerCase() + " DESC LIMIT " + limit);
            preparedStatement.setString(1, temporaryStatisticType.format());
            final ResultSet resultSet = preparedStatement.executeQuery();
            final List<StatisticPlayer> players = new ArrayList<>();

            while (resultSet.next()) {
                final Map<StatisticType, Integer> stats = new HashMap<>();
                for (StatisticType type : StatisticType.values()) {
                    stats.put(type, resultSet.getInt(type.name().toLowerCase()));
                }
                players.add(new StatisticPlayer(
                    UUID.fromString(resultSet.getString("uuid")),
                    stats
                ));
            }

            return players;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public void resetStats(UUID uuid) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    private void saveStats(UUID uuid, Map<StatisticType, Integer> difference) {
        for (TemporaryStatisticType type : this.statsConfig.getTemporaryStatisticTypes()) {
            try {
                final Connection connection = this.dataSource.getConnection();
                final PreparedStatement statement = connection.prepareStatement(difference.isEmpty() ?
                    "INSERT INTO " + TABLE_NAME + " (`uuid`, `temporarytype`) VALUES (?, ?) ON DUPLICATE KEY UPDATE uuid=uuid;" :
                    "INSERT INTO " + TABLE_NAME + " (`uuid`, `temporarytype`, `" + this.formatType(difference, false, false, false) + "`) VALUES (?, ?, " + this.formatType(difference, true, false, false) + ") ON DUPLICATE KEY UPDATE `" + this.formatType(difference, false, true, false) + ";");
                statement.setString(1, uuid.toString());
                statement.setString(2, type.format());

                int i = 2;
                for (StatisticType statisticType : difference.keySet()) {
                    statement.setInt(++i, difference.get(statisticType));
                }
                for (StatisticType statisticType : difference.keySet()) {
                    statement.setInt(++i, difference.get(statisticType));
                }
                statement.execute();
                statement.close();
                //connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String formatType(Map<StatisticType, Integer> difference, boolean prepared, boolean values, boolean type) {
        final StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        for (StatisticType statisticType : difference.keySet()) {
            stringBuilder.append(prepared ? "?" : statisticType.name().toLowerCase());
            if (values) {
                stringBuilder.append("`=?");
            } else if (type) {
                stringBuilder.append(" int");
            }
            if (++i != difference.keySet().size()) {
                stringBuilder.append(prepared || type ? ", " : values ? ", `" : "`, `");
            }
        }

        return stringBuilder.toString();
    }

    private String formatType(boolean prepared, boolean values, boolean type) {
        final Map<StatisticType, Integer> map = new HashMap<>();
        for (StatisticType statisticType : StatisticType.values()) {
            map.put(statisticType, 0);
        }
        return this.formatType(map, prepared, values, type);
    }

}
