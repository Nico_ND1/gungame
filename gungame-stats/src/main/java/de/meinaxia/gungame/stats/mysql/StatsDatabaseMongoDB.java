package de.meinaxia.gungame.stats.mysql;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import de.meinaxia.gungame.stats.StatisticPlayer;
import de.meinaxia.gungame.stats.StatisticType;
import de.meinaxia.gungame.stats.StatsConfig;
import de.meinaxia.gungame.stats.StatsDatabase;
import de.meinaxia.gungame.stats.TemporaryStatisticType;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Nico_ND1
 */

// WARNING: the code that follows will make you cry; a safety pig is provided below for your benefit.
//
//  _._ _..._ .-',     _.._(`))
// '-. `     '  /-._.-'    ',/
//    )         \            '.
//   / _    _    |             \
//  |  a    a    /              |
//  \   .-.                     ;
//   '-('' ).-'       ,'       ;
//      '-;           |      .'
//         \           \    /
//         | 7  .__  _.-\   \
//         | |  |  ``/  /`  /
//        /,_|  |   /,_/   /
//           /,_/      '`-'
public class StatsDatabaseMongoDB implements StatsDatabase {

    private final MongoClient mongoClient;
    private final MongoDatabase mongoDatabase;
    private final MongoCollection<Document> collection;
    private final Cache<UUID, StatisticPlayer> playerCache = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build();

    public StatsDatabaseMongoDB(StatsConfig config) {
        final CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
            CodecRegistries.fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)),
            MongoClient.getDefaultCodecRegistry()
        );
        final MongoClientOptions clientOptions = MongoClientOptions.builder()
            .codecRegistry(codecRegistry)
            .build();

        this.mongoClient = new MongoClient(
            new ServerAddress(config.getHost(), config.getPort()),
            Collections.singletonList(MongoCredential.createCredential(
                config.getUser(), config.getDatabase(), config.getPassword().toCharArray()
            )), clientOptions
        );
        this.mongoDatabase = this.mongoClient.getDatabase(config.getDatabase());
        this.collection = this.mongoDatabase.getCollection("stats-gungame");

        Logger.getLogger("org.mongodb.driver").setLevel(Level.OFF);
    }

    @Override
    public StatisticPlayer loadPlayer(UUID uuid) {
        final StatisticPlayer statisticPlayer = this.getPlayer(uuid, TemporaryStatisticType.ALL);
        this.playerCache.put(uuid, statisticPlayer);
        return statisticPlayer;
    }

    @Override
    public StatisticPlayer getPlayer(UUID uuid, TemporaryStatisticType temporaryStatisticType) {
        final Document document = this.collection.find(Filters.and(Filters.eq("uuid", uuid.toString()), Filters.eq("temporary-type", temporaryStatisticType.format()))).first();
        if (document == null) {
            return new StatisticPlayer(uuid, new HashMap<>());
        }

        final Map<StatisticType, Integer> stats = new HashMap<>();
        for (StatisticType type : StatisticType.values()) {
            stats.put(type, document.getInteger(type.name()));
        }

        return new StatisticPlayer(uuid, stats);
    }

    @Override
    public void saveStats(StatisticPlayer statisticPlayer) {
        final Document document = new Document("uuid", statisticPlayer.getUuid().toString());
        final Document statsDocument = new Document();
        statisticPlayer.getDifference().forEach((statisticType, integer) -> statsDocument.append(statisticType.name(), integer));
        document.putAll(statsDocument);

        for (TemporaryStatisticType type : TemporaryStatisticType.values()) {
            final Bson filters = Filters.and(Filters.eq("uuid", statisticPlayer.getUuid().toString()), Filters.eq("temporary-type", type.format()));
            final long result = this.collection.updateOne(filters, new Document("$inc", statsDocument)).getMatchedCount();
            if (result == 0) { // no documents updated = doesn't exist
                this.collection.updateOne(filters, new Document("$set", document), new UpdateOptions().upsert(true));
            }
        }
    }

    @Override
    public StatisticPlayer getPlayer(UUID uuid) {
        final StatisticPlayer statisticPlayer = this.playerCache.getIfPresent(uuid);
        if (statisticPlayer == null) {
            return this.loadPlayer(uuid);
        }
        return statisticPlayer;
    }

    @Override
    public List<StatisticPlayer> getTopList(StatisticType statisticType, TemporaryStatisticType temporaryStatisticType, int limit) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    @Override
    public void resetStats(UUID uuid) {
        this.collection.deleteMany(Filters.eq("uuid", uuid.toString()));
    }

}
