package de.meinaxia.gungame.stats;
import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public interface StatsDatabase {

    StatisticPlayer loadPlayer(UUID uuid);

    StatisticPlayer getPlayer(UUID uuid, TemporaryStatisticType temporaryStatisticType);

    void saveStats(StatisticPlayer statisticPlayer);

    StatisticPlayer getPlayer(UUID uuid);

    List<StatisticPlayer> getTopList(StatisticType statisticType, TemporaryStatisticType temporaryStatisticType, int limit);

    void resetStats(UUID uuid);

}
