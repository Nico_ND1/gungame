package de.meinaxia.gungame.stats;
import lombok.Getter;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Nico_ND1
 */
public class StatisticPlayer {

    @Getter private final UUID uuid;
    @Getter private final Map<StatisticType, Integer> difference = new ConcurrentHashMap<>();
    private final Map<StatisticType, Integer> stats;

    public StatisticPlayer(UUID uuid, Map<StatisticType, Integer> stats) {
        this.uuid = uuid;
        this.stats = stats;
        for (StatisticType type : StatisticType.values()) {
            this.difference.put(type, 0);
        }
    }

    public int getDifference(StatisticType type) {
        return this.difference.getOrDefault(type, 0);
    }

    public int getStat(StatisticType type) {
        return this.stats.getOrDefault(type, 0) + this.difference.getOrDefault(type, 0);
    }

    public void increment(StatisticType type, int amount) {
        this.difference.put(type, this.difference.getOrDefault(type, 0) + amount);
    }

    public void increment(StatisticType type) {
        this.increment(type, 1);
    }

}
