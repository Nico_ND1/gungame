package de.meinaxia.gungame.stats;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public enum StatisticType {

    KILLS("Kills"),
    DEATHS("Deaths"),
    BEST_LEVEL("Höchstes Level");

    private final String prettyName;

}
