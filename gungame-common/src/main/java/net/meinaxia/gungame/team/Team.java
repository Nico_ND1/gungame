package net.meinaxia.gungame.team;
import lombok.Getter;
import net.meinaxia.gungame.player.GunGamePlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Getter
public class Team {

    private final UUID id;
    private final List<GunGamePlayer> players;
    private final List<UUID> invites;

    public Team(List<GunGamePlayer> players) {
        this.id = UUID.randomUUID();
        this.players = players;
        this.invites = new ArrayList<>();
    }

    public Team(GunGamePlayer... players) {
        this(new ArrayList<>(Arrays.asList(players)));
    }

    public Team() {
        this(new ArrayList<>());
    }

    public void broadcast(String message) {
        for (GunGamePlayer player : this.players) {
            player.getPlayer().sendMessage(message);
        }
    }

}
