package net.meinaxia.gungame.team;
import lombok.Getter;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Nico_ND1
 */
public class TeamManager {

    @Getter private final Map<UUID, Team> teams = new ConcurrentHashMap<>();

    public void setTeam(GunGamePlayer player, Team newTeam) {
        player.setCurrentTeam(newTeam);
        // TODO: Scoreboard stuff
    }

    public Team getTeam(UUID id) {
        return this.teams.get(id);
    }

    public Team getTeam(GunGamePlayer player) {
        return player.getCurrentTeam();
    }

    public void removeTeam(Team team) {
        this.teams.remove(team.getId());
        for (GunGamePlayer player : team.getPlayers()) {
            player.setCurrentTeam(null);
            player.getPlayer().sendMessage(Messages.TEAM_REMOVED.format());
        }
    }

    public void removeInvalidTeams(int newMaxTeamSize) {
        for (Entry<UUID, Team> entry : this.teams.entrySet()) {
            if (entry.getValue().getPlayers().size() > newMaxTeamSize) {
                final Team team = this.teams.remove(entry.getKey());
                for (GunGamePlayer player : team.getPlayers()) {
                    player.setCurrentTeam(null);
                    player.getPlayer().sendMessage(Messages.TEAM_REMOVED_TOO_BIG.format());
                }
            }
        }
    }

    public boolean canDamage(GunGamePlayer player, GunGamePlayer attacker) {
        final Team playerTeam = this.getTeam(player);
        final Team attackerTeam = this.getTeam(attacker);
        return playerTeam == null || attackerTeam == null || !playerTeam.getId().equals(attackerTeam.getId());
    }

}
