package net.meinaxia.gungame.map;
/**
 * @author Nico_ND1
 */
public interface MapManager {

    Map getCurrentMap();

    void setCurrentMap(Map newMap);

}
