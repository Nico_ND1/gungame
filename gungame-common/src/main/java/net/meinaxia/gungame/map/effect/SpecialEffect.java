package net.meinaxia.gungame.map.effect;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;

import java.lang.reflect.Type;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Getter
public abstract class SpecialEffect implements JsonDeserializer<SpecialEffect>, JsonSerializer<SpecialEffect> {

    @Expose protected final Location origin;
    @Expose private final int period;
    @Expose private final SpecialEffectType effectType;

    public abstract void display();

    @Override
    public JsonElement serialize(SpecialEffect src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.add("origin", this.setLocation(src.origin));
        jsonObject.addProperty("period", src.period);
        jsonObject.addProperty("effectType", src.effectType.name());

        return jsonObject;
    }

    protected JsonObject setLocation(Location location) {
        final JsonObject locationObject = new JsonObject();
        locationObject.addProperty("world", location.getWorld().getName());
        locationObject.addProperty("x", location.getX());
        locationObject.addProperty("y", location.getY());
        locationObject.addProperty("z", location.getZ());
        locationObject.addProperty("yaw", location.getYaw());
        locationObject.addProperty("pitch", location.getPitch());
        return locationObject;
    }

    protected Location getLocation(JsonElement jsonElement) {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final String worldName = jsonObject.get("world").getAsString();
        if (Bukkit.getWorld(worldName) == null) Bukkit.createWorld(new WorldCreator(worldName));
        return new Location(
            Bukkit.getWorld(worldName),
            jsonObject.get("x").getAsDouble(),
            jsonObject.get("y").getAsDouble(),
            jsonObject.get("z").getAsDouble(),
            jsonObject.get("yaw").getAsFloat(),
            jsonObject.get("pitch").getAsFloat()
        );
    }

}
