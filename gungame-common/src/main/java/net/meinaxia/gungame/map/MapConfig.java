package net.meinaxia.gungame.map;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.meinaxia.gungame.map.effect.ParticleSpecialEffect;
import net.meinaxia.gungame.map.effect.SpecialEffect;
import net.meinaxia.gungame.util.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Getter
public class MapConfig implements JsonDeserializer<MapConfig> {

    public static final Gson GSON = new GsonBuilder()
        .registerTypeAdapter(Map.class, new Map())
        .registerTypeAdapter(MapConfig.class, new MapConfig())
        .registerTypeAdapter(ParticleSpecialEffect.class, new ParticleSpecialEffect())
        .setPrettyPrinting()
        .serializeNulls()
        .create();

    @Expose private final List<Map> maps;

    public static MapConfig loadConfig(String path) throws FileNotFoundException {
        return MapConfig.GSON.fromJson(new InputStreamReader(new FileInputStream(mkdirPath(path)), Charset.forName("UTF-8")), MapConfig.class);
    }

    private static File mkdirPath(String path) {
        final File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new MapConfig(Collections.singletonList(new Map("test", new Location(Bukkit.getWorld("world"), 0, 0, 0), 1, new Cuboid(Bukkit.getWorld("world"), 0, 0, 0, 1, 1, 1), Collections.singletonList(new ParticleSpecialEffect(new Location(Bukkit.getWorld("world"), 0, 0, 0), 5, ParticleEffect.HEART, .1f, 1, 1, 1, 1))))).save(path);
        }
        return file;
    }

    @Override
    public MapConfig deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final List<Map> maps = new ArrayList<>();
        jsonObject.getAsJsonArray("maps").forEach(object -> {
            final Map map = context.deserialize(object, Map.class);
            maps.add(map);

            for (SpecialEffect specialEffect : map.getSpecialEffects()) {
                Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("gungame"), specialEffect::display, 0, specialEffect.getPeriod());
            }
        });

        return new MapConfig(maps);
    }

    public void save(String path) {
        final File file = MapConfig.mkdirPath(path);

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(MapConfig.GSON.toJson(this, MapConfig.class));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
