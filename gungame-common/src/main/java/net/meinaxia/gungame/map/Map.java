package net.meinaxia.gungame.map;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.meinaxia.gungame.map.effect.SpecialEffect;
import net.meinaxia.gungame.map.effect.SpecialEffectType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class Map implements JsonDeserializer<Map>, JsonSerializer<Map> {

    private final String name;
    private final Location spawnLocation;
    private final int maxTeamSize;
    private final Cuboid cuboid;
    private final List<SpecialEffect> specialEffects;

    @Override
    public Map deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final Cuboid cuboid = new Cuboid(
            this.getLocation(jsonObject.getAsJsonObject("spawn").get("lower")),
            this.getLocation(jsonObject.getAsJsonObject("spawn").get("upper"))
        );
        final List<SpecialEffect> specialEffects = new ArrayList<>();
        for (JsonElement jsonElement : jsonObject.getAsJsonArray("specialEffects")) {
            final SpecialEffectType type = SpecialEffectType.valueOf(jsonElement.getAsJsonObject().get("effectType").getAsString());
            specialEffects.add(context.deserialize(jsonElement, type.getClazz()));
        }

        return new Map(
            jsonObject.get("name").getAsString(),
            this.getLocation(jsonObject.getAsJsonObject("spawnLocation")),
            jsonObject.get("maxTeamSize").getAsInt(),
            cuboid,
            specialEffects
        );
    }

    @Override
    public JsonElement serialize(Map src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", src.name);
        jsonObject.addProperty("maxTeamSize", src.maxTeamSize);

        final JsonObject spawnObject = new JsonObject();
        spawnObject.add("lower", this.setLocation(src.cuboid.getLowerNE()));
        spawnObject.add("upper", this.setLocation(src.cuboid.getUpperSW()));

        jsonObject.add("spawn", spawnObject);
        jsonObject.add("spawnLocation", this.setLocation(src.spawnLocation));
        jsonObject.add("specialEffects", MapConfig.GSON.toJsonTree(src.specialEffects));

        return jsonObject;
    }

    private JsonObject setLocation(Location location) {
        final JsonObject locationObject = new JsonObject();
        locationObject.addProperty("world", location.getWorld().getName());
        locationObject.addProperty("x", location.getX());
        locationObject.addProperty("y", location.getY());
        locationObject.addProperty("z", location.getZ());
        locationObject.addProperty("yaw", location.getYaw());
        locationObject.addProperty("pitch", location.getPitch());
        return locationObject;
    }

    private Location getLocation(JsonElement jsonElement) {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final String worldName = jsonObject.get("world").getAsString();
        if (Bukkit.getWorld(worldName) == null) Bukkit.createWorld(new WorldCreator(worldName));
        return new Location(
            Bukkit.getWorld(worldName),
            jsonObject.get("x").getAsDouble(),
            jsonObject.get("y").getAsDouble(),
            jsonObject.get("z").getAsDouble(),
            jsonObject.get("yaw").getAsFloat(),
            jsonObject.get("pitch").getAsFloat()
        );
    }

}
