package net.meinaxia.gungame.map.effect;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public enum SpecialEffectType {

    PARTICLE(ParticleSpecialEffect.class);

    private final Class<? extends SpecialEffect> clazz;

}
