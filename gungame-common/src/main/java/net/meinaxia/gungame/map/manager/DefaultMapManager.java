package net.meinaxia.gungame.map.manager;
import de.dytanic.cloudnet.bridge.CloudServer;
import lombok.Getter;
import lombok.Setter;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.map.Map;
import net.meinaxia.gungame.map.MapConfig;
import net.meinaxia.gungame.map.MapManager;
import net.meinaxia.gungame.player.GunGamePlayer;

import java.util.Random;

/**
 * @author Nico_ND1
 */
public class DefaultMapManager implements MapManager {

    @Setter private GunGame gunGame;
    @Getter private Map currentMap;

    public DefaultMapManager(MapConfig mapConfig) {
        if (!mapConfig.getMaps().isEmpty()) {
            this.currentMap = mapConfig.getMaps().get(new Random().nextInt(mapConfig.getMaps().size()));
            if (CloudServer.getInstance() != null) {
                CloudServer.getInstance().setMotdAndUpdate(this.currentMap.getName());
            }
        }
    }

    @Override
    public void setCurrentMap(Map currentMap) {
        if (this.currentMap.getMaxTeamSize() != currentMap.getMaxTeamSize()) {
            this.gunGame.getTeamManager().removeInvalidTeams(currentMap.getMaxTeamSize());
        }

        this.currentMap = currentMap;
        CloudServer.getInstance().setMotdAndUpdate(this.currentMap.getName());

        for (GunGamePlayer value : this.gunGame.getPlayers().values()) {
            value.getScoreboard().update(value);
        }
    }
}
