package net.meinaxia.gungame.map.effect;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.annotations.Expose;
import lombok.NoArgsConstructor;
import net.meinaxia.gungame.util.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.lang.reflect.Type;

/**
 * @author Nico_ND1
 */
@NoArgsConstructor(force = true)
public class ParticleSpecialEffect extends SpecialEffect {

    @Expose private final ParticleEffect particleEffect;
    @Expose private final float speed;
    @Expose private final int amount;
    @Expose private final float offsetX;
    @Expose private final float offsetY;
    @Expose private final float offsetZ;

    public ParticleSpecialEffect(Location location, int period, ParticleEffect particleEffect, float speed, int amount,
                                 float offsetX, float offsetY, float offsetZ) {
        super(location, period, SpecialEffectType.PARTICLE);
        this.particleEffect = particleEffect;
        this.speed = speed;
        this.amount = amount;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.offsetZ = offsetZ;
    }

    @Override
    public void display() {
        try {
            this.particleEffect.sendToPlayers(
                Bukkit.getOnlinePlayers(),
                this.origin,
                this.offsetX,
                this.offsetY,
                this.offsetZ,
                this.speed,
                this.amount
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public JsonElement serialize(SpecialEffect src, Type typeOfSrc, JsonSerializationContext context) {
        final ParticleSpecialEffect effect = (ParticleSpecialEffect) src;
        final JsonObject jsonObject = (JsonObject) super.serialize(src, typeOfSrc, context);
        jsonObject.addProperty("particleEffect", effect.particleEffect.name());
        jsonObject.addProperty("speed", effect.speed);
        jsonObject.addProperty("amount", effect.amount);
        jsonObject.addProperty("offsetX", effect.offsetX);
        jsonObject.addProperty("offsetY", effect.offsetY);
        jsonObject.addProperty("offsetZ", effect.offsetZ);

        return jsonObject;
    }

    @Override
    public SpecialEffect deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final Location origin = this.getLocation(jsonObject.get("origin"));
        final int period = jsonObject.get("period").getAsInt();
        final ParticleEffect particleEffect = ParticleEffect.valueOf(jsonObject.get("particleEffect").getAsString());
        final float speed = jsonObject.get("speed").getAsFloat();
        final int amount = jsonObject.get("amount").getAsInt();
        final float offsetX = jsonObject.get("offsetX").getAsFloat();
        final float offsetY = jsonObject.get("offsetY").getAsFloat();
        final float offsetZ = jsonObject.get("offsetZ").getAsFloat();

        return new ParticleSpecialEffect(origin, period, particleEffect, speed, amount, offsetX, offsetY, offsetZ);
    }
}
