package net.meinaxia.gungame.player;
import de.meinaxia.gungame.stats.StatisticType;
import lombok.Getter;
import net.meinaxia.gungame.GunGame;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author Nico_ND1
 */
public class GunGameScoreboard {

    private static final String[] CODES = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    private final GunGame gunGame;
    @Getter private final Scoreboard scoreboard;
    private final Objective objective;

    public GunGameScoreboard(GunGame gunGame, Scoreboard scoreboard) {
        this.gunGame = gunGame;
        this.scoreboard = scoreboard;
        this.objective = scoreboard.getObjective("stats") == null ?
            scoreboard.registerNewObjective("stats", "dummy") :
            scoreboard.getObjective("stats");
        this.objective.setDisplayName("§8● §5§lGunGame §8●");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        this.addTeam("empty1", "§a§b", 11);
        this.addTeam("level1", "§d❉ §8| §dLevel", 10);
        this.addTeam("level2", "§6§e", 9);
        this.addTeam("empty2", "§b§a", 8);
        this.addTeam("kills1", "§d✣ §8| §dKills", 7);
        this.addTeam("kills2", "§e§e", 6);
        this.addTeam("empty3", "§c§c", 5);
        this.addTeam("map1", "§d✰ §8| §dMap", 4);
        this.addTeam("map2", "§d§e", 3);
        this.addTeam("empty4", "§d§d", 2);
        this.addTeam("record1", "§d❁ §8| §dLevelrekord", 1);
        this.addTeam("record2", "§c§e", 0);
    }

    private void addTeam(String name, String display, int order) {
        final Score score = this.objective.getScore("§" + CODES[order]);
        if (!score.isScoreSet()) score.setScore(order);
        final Team team = this.scoreboard.getTeam(name) == null ? this.scoreboard.registerNewTeam(name) : this.scoreboard.getTeam(name);
        if (!team.hasEntry("§" + CODES[order])) {
            team.addEntry("§" + CODES[order]);
        }

        if (display.length() > 16) {
            team.setPrefix(display.substring(0, 16));

            final String substring = ChatColor.getLastColors(display.substring(0, 16)) + display.substring(16);
            team.setSuffix(substring);
        } else {
            team.setPrefix(display);
        }
    }

    public void update(GunGamePlayer gunGamePlayer) {
        this.addTeam("level2", "§8➥ §5" + gunGamePlayer.getLevel(), 9);
        this.addTeam("kills2", "§8➥ §5" + gunGamePlayer.getStatistics().getStat(StatisticType.KILLS), 6);
        this.addTeam("map2", "§8➥ §5" + this.gunGame.getMapManager().getCurrentMap().getName(), 3);
        this.addTeam("record2", "§8➥ §5" + gunGamePlayer.getStatistics().getStat(StatisticType.BEST_LEVEL), 0);
    }

}
