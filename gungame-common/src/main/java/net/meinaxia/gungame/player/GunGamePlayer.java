package net.meinaxia.gungame.player;
import de.meinaxia.gungame.stats.StatisticPlayer;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.meinaxia.gungame.team.Team;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class GunGamePlayer {

    private final UUID uuid;
    private final Player player;
    private StatisticPlayer statistics;
    private int level;
    private int killingSpree;
    private boolean receiveTeamInvites;
    private Team currentTeam;
    private final GunGameScoreboard scoreboard;
    private boolean spectator;

    public void incrementLevel(int level) {
        this.level += level;
    }

    public void incrementKillingSpree(int spree) {
        this.killingSpree += spree;
    }

    public boolean announceKillingSpree() {
        return this.killingSpree != 0 && this.killingSpree <= 15 ? this.killingSpree % 5 == 0 : this.killingSpree % 10 == 0;
    }

}
