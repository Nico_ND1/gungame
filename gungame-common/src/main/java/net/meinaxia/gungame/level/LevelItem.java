package net.meinaxia.gungame.level;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class LevelItem {

    @Getter private final int slot;
    private final Material material;
    private final int amount;
    private final short data;
    private final String name;
    private final short durability;
    private final List<ConfigEnchantment> enchantments;

    public LevelItem(JsonObject jsonObject) {
        this.slot = jsonObject.get("slot").getAsInt();
        this.material = Material.valueOf(jsonObject.get("material").getAsString());
        this.amount = jsonObject.get("amount").getAsInt();
        this.name = jsonObject.has("name") ? jsonObject.get("name").getAsString() : null;
        this.data = jsonObject.get("data").getAsShort();
        this.durability = jsonObject.get("durability").getAsShort();
        this.enchantments = new ArrayList<>();
        jsonObject.getAsJsonArray("enchantments").forEach(jsonElement -> {
            final JsonObject enchantmentObject = jsonElement.getAsJsonObject();
            this.enchantments.add(new ConfigEnchantment(enchantmentObject.get("name").getAsString(), enchantmentObject.get("level").getAsInt()));
        });
    }

    public LevelItem(int slot, ItemStack itemStack) {
        this.slot = slot;
        this.material = itemStack.getType();
        this.amount = itemStack.getAmount();
        this.data = itemStack.getData().getData();
        this.name = itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName() ? itemStack.getItemMeta().getDisplayName() : null;
        this.durability = itemStack.getDurability();
        this.enchantments = new ArrayList<>();
        itemStack.getEnchantments().forEach((enchantment, integer) -> this.enchantments.add(new ConfigEnchantment(enchantment.getName(), integer)));
    }

    public JsonObject toJsonObject() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("slot", this.slot);
        jsonObject.addProperty("material", this.material.name());
        jsonObject.addProperty("amount", this.amount);
        jsonObject.addProperty("data", this.data);
        jsonObject.addProperty("name", this.name);
        jsonObject.addProperty("durability", this.durability);
        jsonObject.add("enchantments", new Gson().toJsonTree(this.enchantments.stream()
            .map(configEnchantment -> {
                final JsonObject enchantmentObject = new JsonObject();
                enchantmentObject.addProperty("name", configEnchantment.name);
                enchantmentObject.addProperty("level", configEnchantment.level);
                return (JsonElement) enchantmentObject;
            }).collect(Collectors.toList())));

        return jsonObject;
    }

    public ItemStack toItemStack() {
        final ItemStack itemStack = new ItemStack(this.material, this.amount, this.data);
        itemStack.setDurability(this.durability);

        final ItemMeta itemMeta = itemStack.getItemMeta();
        if(itemMeta != null) {
            itemMeta.spigot().setUnbreakable(true);
            for (ConfigEnchantment enchantment : this.enchantments) {
                itemMeta.addEnchant(Enchantment.getByName(enchantment.name), enchantment.level, true);
            }
            if (this.name != null) itemMeta.setDisplayName(this.name);
            itemStack.setItemMeta(itemMeta);
        }

        return itemStack;
    }

    @AllArgsConstructor
    @Data
    public class ConfigEnchantment {

        private final String name;
        private final int level;

    }

}
