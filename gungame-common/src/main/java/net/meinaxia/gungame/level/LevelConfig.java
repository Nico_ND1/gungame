package net.meinaxia.gungame.level;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Getter
public class LevelConfig implements JsonDeserializer<LevelConfig> {

    private static final Gson GSON = new GsonBuilder()
        .registerTypeAdapter(Level.class, new Level())
        .registerTypeAdapter(LevelConfig.class, new LevelConfig())
        .serializeNulls()
        .setPrettyPrinting()
        .create();

    @Expose private final List<Level> levels;
    @Expose @Setter private int maxLevels;

    public static LevelConfig loadConfig(String path) throws FileNotFoundException {
        return LevelConfig.GSON.fromJson(new InputStreamReader(new FileInputStream(mkdirPath(path)), Charset.forName("UTF-8")), LevelConfig.class);
    }

    private static File mkdirPath(String path) {
        final File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new LevelConfig(Collections.singletonList(new Level(0, Collections.singletonList(new LevelItem(0, new ItemStack(Material.WOOD_AXE))))), 4).save(path);
        }
        return file;
    }

    @Override
    public LevelConfig deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();

        final List<Level> levels = new ArrayList<>();
        final AtomicInteger counter = new AtomicInteger(0);
        jsonObject.getAsJsonArray("levels").forEach(object -> {
            final Level level = context.deserialize(object, Level.class);
            level.setLevel(counter.getAndIncrement());
            levels.add(level);
        });

        return new LevelConfig(levels, jsonObject.get("maxLevels").getAsInt());
    }

    public void save(String path) {
        final File file = LevelConfig.mkdirPath(path);

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(LevelConfig.GSON.toJson(this, LevelConfig.class));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
