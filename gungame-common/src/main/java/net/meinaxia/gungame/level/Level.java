package net.meinaxia.gungame.level;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.entity.Player;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class Level implements JsonDeserializer<Level>, JsonSerializer<Level> {

    private int level;
    private final List<LevelItem> levelItems;

    public void applyItems(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);

        this.levelItems.forEach(levelItem -> {
            player.getInventory().setItem(levelItem.getSlot(), levelItem.toItemStack());
        });
        player.updateInventory();
    }

    @Override
    public Level deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final List<LevelItem> levelItems = new ArrayList<>();
        jsonObject.getAsJsonArray("items").forEach(object -> {
            levelItems.add(new LevelItem(object.getAsJsonObject()));
        });

        return new Level(-1, levelItems);
    }

    @Override
    public JsonElement serialize(Level src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.add("items", new Gson().toJsonTree(src.levelItems.stream()
            .map(LevelItem::toJsonObject)
            .collect(Collectors.toList())));
        return jsonObject;
    }
}
