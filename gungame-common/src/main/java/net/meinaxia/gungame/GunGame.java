package net.meinaxia.gungame;
import de.meinaxia.gungame.stats.StatsDatabase;
import lombok.Getter;
import net.meinaxia.gungame.level.Level;
import net.meinaxia.gungame.level.LevelConfig;
import net.meinaxia.gungame.map.MapManager;
import net.meinaxia.gungame.player.GunGamePlayer;
import net.meinaxia.gungame.team.Team;
import net.meinaxia.gungame.team.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Getter
public class GunGame {

    private final Map<UUID, GunGamePlayer> players = new HashMap<>();
    private final Map<Integer, Level> levels = new HashMap<>();
    private final LevelConfig levelConfig;
    private final MapManager mapManager;
    private final StatsDatabase statsDatabase;
    private final TeamManager teamManager;

    public GunGame(LevelConfig levelConfig, MapManager mapManager, StatsDatabase statsDatabase) {
        this.levelConfig = levelConfig;
        levelConfig.getLevels().forEach(level -> this.levels.put(level.getLevel(), level));
        this.mapManager = mapManager;
        this.statsDatabase = statsDatabase;
        this.teamManager = new TeamManager();
    }

    public GunGamePlayer getPlayer(UUID uuid) {
        return this.players.get(uuid);
    }

    public GunGamePlayer getPlayer(Player player) {
        return this.players.get(player.getUniqueId());
    }

    public void setPlayer(GunGamePlayer player) {
        this.players.put(player.getUuid(), player);
    }

    public GunGamePlayer removePlayer(UUID uuid) {
        final GunGamePlayer gunGamePlayer = this.players.remove(uuid);
        final Team team = gunGamePlayer.getCurrentTeam();
        if (team == null) return gunGamePlayer;

        team.getPlayers().remove(gunGamePlayer);
        team.getInvites().remove(uuid);

        if (team.getPlayers().isEmpty()) {
            this.teamManager.removeTeam(team);
        }
        return gunGamePlayer;
    }

    public Level getLevel(int level) {
        return this.levels.get(level);
    }

    public boolean useNick() {
        return Bukkit.getPluginManager().isPluginEnabled("NickSystem");
    }

}
