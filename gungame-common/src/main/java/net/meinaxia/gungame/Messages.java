package net.meinaxia.gungame;
/**
 * @author Nico_ND1
 */
public enum Messages {

    JOIN("{0}{1} §7hat das Spiel betreten."),
    QUIT("{0}{1} §7hat das Spiel verlassen."),
    TEAM_REMOVED("§cDas Team wurde aufgelöst."),
    TEAM_REMOVED_TOO_BIG("§cDas Team wurde aufgrund der Teamgröße aufgelöst."),
    STATS_COMMAND_ERROR("§cEs trat ein Fehler auf."),
    STATS_COMMAND_RESULT_HEADER("Statistiken von §e{0}"),
    STATS_COMMAND_RESULT_STAT("§e{0}§7: {1}"),
    STATS_COMMAND_RESULT_KD("§eK/D§7: {0}"),
    DEATH_KILLING_SPREE("{0}{1} §7ist auf einer Tötungs-Serie von §a{2} Kills§7."),
    DEATH("§c{0}{1} §7starb."),
    DEATH_KILLER("§c{0}{1} §7wurde von §a{2}{3} §7getötet."),
    TEAM_COMMAND_TARGET_OFFLINE("§cDer Spieler ist nicht auf diesem Server."),
    TEAM_COMMAND_INVITE_TARGET_NOT_RECEIVING("§cDer Spieler erhält keine Team-Anfragen."),
    TEAM_COMMAND_INVITATION("§e{0}{1} §7hat §a{2}{3} §7in das Team eingeladen."),
    TEAM_COMMAND_INVITED("Du hast den Spieler in das Team eingeladen."),
    TEAM_COMMAND_INVITATION_RECEIVED("Du hast eine Team-Anfrage von §e{0}{1} §7erhalten.\n "),
    TEAM_COMMAND_ALREADY_IN_TEAM("§cDu bist bereits in einem Team."),
    TEAM_COMMAND_ID_INVALID("§cGib eine gültige Id an."),
    TEAM_COMMAND_TEAM_NOT_FOUND("§cDas Team wurde nicht gefunden."),
    TEAM_COMMAND_INVITATION_NOT_RECEIVED("§cDu hast keine Anfrage von diesem Team erhalten."),
    TEAM_COMMAND_INVITATION_ACCEPTED_BROADCAST("§a{0}{1} §7hat das Team betreten."),
    TEAM_COMMAND_TOO_BIG("§cDas Team ist zu groß."),
    TEAM_COMMAND_INVITATION_DECLINED("Du hast die Team-Anfrage abgelehnt."),
    TEAM_COMMAND_TARGET_SELF("§cDu kannst dich nicht selbst einladen."),
    TEAM_COMMAND_LEAVE_NOT_IN_TEAM("§cDu bist in keinem Team."),
    TEAM_COMMAND_LEAVE_SUCCESS_BROADCAST("§c{0}{1} §7hat das Team verlassen."),
    TEAM_COMMAND_LEAVE_SUCCESS("Du hast das Team verlassen."),
    TEAM_COMMAND_TOGGLE("Du kannst nun {0}Team-Anfragen erhalten§7."),
    TEAM_COMMAND_INFO("§9Mitglieder:{0}"),
    TEAM_COMMAND_NOT_SAME_TEAM("§cIhr müsst in dem selben Team sein."),
    TEAM_COMMAND_KICKED("§e{0} §7wurde von §a{1} §7gekickt."),
    TEAM_COMMAND_USAGE("§cBenutzung: §7/team <invite, accept, deny, kick> <Spieler> §coder §7/team <info, leave, toggle>"),
    SPECTATOR_COMMAND_TOGGLE("Du bist nun {0}Spectator§7."),
    TELEPORT_COMMAND_USAGE("§cBenutzung: §7/teleport <Name>"),
    TELEPORT_COMMAND_NOT_ONLINE("§cDer Spieler ist nicht online."),
    TELEPORT_COMMAND_NOT_SPECTATOR("§cDu musst dafür ein Spectator sein."),
    SETLEVEL_COMMAND_LEVEL_INVALID("§cDieses Level ist nicht verfügbar."),
    SETLEVEL_COMMAND_SUCCESS("§aDu hast das Level erfolgreich geändert."),
    SETLEVEL_COMMAND_USAGE("§cBenutzung: §7/setlevel [Name] <Level>"),
    RESETSTATS_COMMAND_SUCCESS("§aDu hast die Stats von dem Spieler erfolgreich resetted."),
    RESETSTATS_COMMAND_USAGE("§cBenutzung: §7/resetstats <Name>");

    private final String message;

    Messages(boolean prefix, String message) {
        if (prefix) {
            this.message = "§d╿◗ §5GunGame §8> §7" + message;
        } else {
            this.message = message;
        }
    }

    Messages(String message) {
        this(true, message);
    }

    public String format(Object... args) {
        String result = this.message;
        for (int i = 0; i < args.length; i++) {
            result = result.replace("{" + i + "}", String.valueOf(args[i]));
        }

        return result;
    }

}
