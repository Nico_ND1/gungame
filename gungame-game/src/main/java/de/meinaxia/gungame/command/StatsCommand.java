package de.meinaxia.gungame.command;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.meinaxia.gungame.GunGamePlugin;
import de.meinaxia.gungame.stats.StatisticPlayer;
import de.meinaxia.gungame.stats.StatisticType;
import de.meinaxia.gungame.stats.TemporaryStatisticType;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

/**
 * @author Nico_ND1
 */

// WARNING: the code that follows will make you cry; a safety pig is provided below for your benefit.
//
//  _._ _..._ .-',     _.._(`))
// '-. `     '  /-._.-'    ',/
//    )         \            '.
//   / _    _    |             \
//  |  a    a    /              |
//  \   .-.                     ;
//   '-('' ).-'       ,'       ;
//      '-;           |      .'
//         \           \    /
//         | 7  .__  _.-\   \
//         | |  |  ``/  /`  /
//        /,_|  |   /,_/   /
//           /,_/      '`-'
@AllArgsConstructor
public class StatsCommand implements CommandExecutor {

    private final GunGame gunGame;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;

        Bukkit.getScheduler().runTaskAsynchronously(JavaPlugin.getPlugin(GunGamePlugin.class), () -> {
            final String name = args.length == 1 ? args[0] : player.getName();
            try {
                final UUID uuid = args.length == 1 ? this.fetchUUID(name) : player.getUniqueId();
                final StatisticPlayer statisticPlayer = args.length == 1 ? this.gunGame.getStatsDatabase().getPlayer(uuid, TemporaryStatisticType.ALL) : this.gunGame.getPlayer(player).getStatistics();
                if (statisticPlayer == null) {
                    player.sendMessage(Messages.STATS_COMMAND_ERROR.format());
                    return;
                }

                player.sendMessage(Messages.STATS_COMMAND_RESULT_HEADER.format(args.length == 1 ? name : "dir"));
                for (StatisticType type : StatisticType.values()) {
                    player.sendMessage(Messages.STATS_COMMAND_RESULT_STAT.format(type.getPrettyName(), statisticPlayer.getStat(type)));
                }

                final Object kd = statisticPlayer.getStat(StatisticType.KILLS) == 0 ?
                    "0.0" :
                    this.round((double) statisticPlayer.getStat(StatisticType.KILLS) / (double) statisticPlayer.getStat(StatisticType.DEATHS));
                player.sendMessage(Messages.STATS_COMMAND_RESULT_KD.format(kd));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return false;
    }

    private double round(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    private UUID fetchUUID(String playerName) throws Exception {
        // Get response from Mojang API
        URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + playerName);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        if (connection.getResponseCode() == 400) {
            System.err.println("There is no player with the name \"" + playerName + "\"!");
            return UUID.randomUUID();
        }

        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        // Parse JSON response and get UUID
        JsonElement element = new JsonParser().parse(bufferedReader);
        JsonObject object = element.getAsJsonObject();
        String uuidAsString = object.get("id").getAsString();

        // Return UUID
        return this.parseUUIDFromString(uuidAsString);
    }

    private UUID parseUUIDFromString(String uuidAsString) {
        String[] parts = {
            "0x" + uuidAsString.substring(0, 8),
            "0x" + uuidAsString.substring(8, 12),
            "0x" + uuidAsString.substring(12, 16),
            "0x" + uuidAsString.substring(16, 20),
            "0x" + uuidAsString.substring(20, 32)
        };

        long mostSigBits = Long.decode(parts[0]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[1]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[2]);

        long leastSigBits = Long.decode(parts[3]);
        leastSigBits <<= 48;
        leastSigBits |= Long.decode(parts[4]);

        return new UUID(mostSigBits, leastSigBits);
    }

}
