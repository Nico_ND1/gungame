package de.meinaxia.gungame.command;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class SetLevelCommand implements CommandExecutor {

    private final GunGame gunGame;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        if (args.length != 1 && args.length != 2) {
            player.sendMessage(Messages.SETLEVEL_COMMAND_USAGE.format());
            return false;
        }

        final String name = args.length == 2 ? args[0] : player.getName();
        final Player target = Bukkit.getPlayer(name);
        if (target == null || !target.isOnline()) {
            player.sendMessage(Messages.TEAM_COMMAND_TARGET_OFFLINE.format());
            return false;
        }

        final int level = Integer.valueOf(args[1]);
        if (level < 0 || level > this.gunGame.getLevelConfig().getMaxLevels()) {
            player.sendMessage(Messages.SETLEVEL_COMMAND_LEVEL_INVALID.format());
            return false;
        }

        final GunGamePlayer targetGamePlayer = this.gunGame.getPlayer(target);
        targetGamePlayer.setLevel(level);
        target.setLevel(level);
        this.gunGame.getLevel(level).applyItems(target);
        player.sendMessage(Messages.SETLEVEL_COMMAND_SUCCESS.format());

        return false;
    }
}
