package de.meinaxia.gungame.command;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class TeleportCommand implements CommandExecutor {

    private final GunGame gunGame;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        if (args.length != 1) {
            player.sendMessage(Messages.TELEPORT_COMMAND_USAGE.format());
            return false;
        }

        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player);
        if (!gunGamePlayer.isSpectator()) {
            player.sendMessage(Messages.TELEPORT_COMMAND_NOT_SPECTATOR.format());
            return false;
        }

        final Player target = Bukkit.getPlayer(args[0]);
        if (target == null || !target.isOnline()) {
            player.sendMessage(Messages.TELEPORT_COMMAND_NOT_ONLINE.format());
            return false;
        }

        player.teleport(target.getLocation());
        return false;
    }
}
