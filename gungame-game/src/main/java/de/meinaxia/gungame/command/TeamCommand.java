package de.meinaxia.gungame.command;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.map.MapManager;
import net.meinaxia.gungame.player.GunGamePlayer;
import net.meinaxia.gungame.team.Team;
import net.meinaxia.gungame.team.TeamManager;
import net.sasexception.nicksystem.api.NickAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class TeamCommand implements CommandExecutor {

    private final GunGame gunGame;
    private final TeamManager teamManager;
    private final MapManager mapManager;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player);
        Team team = gunGamePlayer.getCurrentTeam();

        /*
        team invite <Spieler>
        team accept <Id>
        team deny <Id>
        team info
        team leave
        team kick <Spieler>
        team toggle
         */

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("invite")) {
                if (team == null) {
                    team = new Team(gunGamePlayer);
                    this.teamManager.setTeam(gunGamePlayer, team);
                    this.teamManager.getTeams().put(team.getId(), team);
                }

                final Player target = Bukkit.getPlayer(args[1]);
                if (target == null || !target.isOnline()) {
                    player.sendMessage(Messages.TEAM_COMMAND_TARGET_OFFLINE.format());
                    return false;
                }
                if (target.getUniqueId().equals(player.getUniqueId())) {
                    player.sendMessage(Messages.TEAM_COMMAND_TARGET_SELF.format());
                    return false;
                }

                final GunGamePlayer gunGameTarget = this.gunGame.getPlayer(target);
                if (!gunGameTarget.isReceiveTeamInvites()) {
                    player.sendMessage(Messages.TEAM_COMMAND_INVITE_TARGET_NOT_RECEIVING.format());
                    return false;
                }

                team.getInvites().add(target.getUniqueId());
                team.broadcast(Messages.TEAM_COMMAND_INVITATION.format(NickAPI.getNickRank(player).getChatColor(), player.getName(), NickAPI.getNickRank(target).getChatColor(), target.getName()));
                player.sendMessage(Messages.TEAM_COMMAND_INVITED.format());

                final ComponentBuilder componentBuilder = new ComponentBuilder(Messages.TEAM_COMMAND_INVITATION_RECEIVED.format(NickAPI.getNickRank(player).getChatColor(), player.getName()))
                    .append("[Annehmen]").color(ChatColor.GREEN).event(new ClickEvent(Action.RUN_COMMAND, "/team accept " + team.getId().toString()))
                    .append(" ")
                    .append("[Ablehnen]").color(ChatColor.RED).event(new ClickEvent(Action.RUN_COMMAND, "/team deny " + team.getId().toString()));
                target.spigot().sendMessage(componentBuilder.create());
                return true;
            } else if (args[0].equalsIgnoreCase("accept") || args[0].equalsIgnoreCase("deny")) {
                if (team != null) {
                    player.sendMessage(Messages.TEAM_COMMAND_ALREADY_IN_TEAM.format());
                    return false;
                }

                final String uuidRegex = "([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}";
                final String idString = args[1];
                if (!idString.matches(uuidRegex)) {
                    player.sendMessage(Messages.TEAM_COMMAND_ID_INVALID.format());
                    return false;
                }

                final UUID id = UUID.fromString(idString);
                final Team foundTeam = this.teamManager.getTeam(id);
                if (foundTeam == null) {
                    player.sendMessage(Messages.TEAM_COMMAND_TEAM_NOT_FOUND.format());
                    return false;
                }
                if (!foundTeam.getInvites().contains(player.getUniqueId())) {
                    player.sendMessage(Messages.TEAM_COMMAND_INVITATION_NOT_RECEIVED.format());
                    return false;
                }

                if (args[0].equalsIgnoreCase("accept")) {
                    if (foundTeam.getPlayers().size() >= this.mapManager.getCurrentMap().getMaxTeamSize()) {
                        player.sendMessage(Messages.TEAM_COMMAND_TOO_BIG.format());
                        return false;
                    }

                    foundTeam.getInvites().remove(player.getUniqueId());
                    foundTeam.getPlayers().add(gunGamePlayer);
                    foundTeam.broadcast(Messages.TEAM_COMMAND_INVITATION_ACCEPTED_BROADCAST.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
                    this.teamManager.setTeam(gunGamePlayer, foundTeam);
                } else {
                    foundTeam.getInvites().remove(player.getUniqueId());
                    player.sendMessage(Messages.TEAM_COMMAND_INVITATION_DECLINED.format());
                }
                return true;
            } else if (args[0].equalsIgnoreCase("kick")) {
                final String name = args[1];
                final Player target = Bukkit.getPlayer(name);
                if (target == null || !target.isOnline()) {
                    player.sendMessage(Messages.TEAM_COMMAND_TARGET_OFFLINE.format());
                    return false;
                }

                final GunGamePlayer targetGamePlayer = this.gunGame.getPlayer(target);
                final Team targetTeam = targetGamePlayer.getCurrentTeam();
                if (targetTeam == null || !targetTeam.getId().equals(team.getId())) {
                    player.sendMessage(Messages.TEAM_COMMAND_NOT_SAME_TEAM.format());
                    return false;
                }

                team.getPlayers().remove(targetGamePlayer);
                team.broadcast(Messages.TEAM_COMMAND_KICKED.format(NickAPI.getNickRank(target).getChatColor() + target.getName(), NickAPI.getNickRank(player).getChatColor() + player.getName()));
                this.verifyTeam(team);
                return true;
            }
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("info")) {
                if (team == null) {
                    player.sendMessage(Messages.TEAM_COMMAND_LEAVE_NOT_IN_TEAM.format());
                    return false;
                }

                player.sendMessage(Messages.TEAM_COMMAND_INFO.format(String.join("\n§7- §e", team.getPlayers().stream()
                    .map(ggPlayer -> ggPlayer.getPlayer().getDisplayName())
                    .collect(Collectors.toList()))));
                return true;
            } else if (args[0].equalsIgnoreCase("leave")) {
                if (team == null) {
                    player.sendMessage(Messages.TEAM_COMMAND_LEAVE_NOT_IN_TEAM.format());
                    return false;
                }

                team.getPlayers().remove(gunGamePlayer);
                gunGamePlayer.setCurrentTeam(null);
                team.broadcast(Messages.TEAM_COMMAND_LEAVE_SUCCESS_BROADCAST.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
                player.sendMessage(Messages.TEAM_COMMAND_LEAVE_SUCCESS.format());
                this.verifyTeam(team);
                // TODO: Scoreboard stuff
                return true;
            } else if (args[0].equalsIgnoreCase("toggle")) {
                gunGamePlayer.setReceiveTeamInvites(!gunGamePlayer.isReceiveTeamInvites());
                player.sendMessage(Messages.TEAM_COMMAND_TOGGLE.format(!gunGamePlayer.isReceiveTeamInvites() ? "§ckeine " : "§a"));
                return true;
            }
        }

        player.sendMessage(Messages.TEAM_COMMAND_USAGE.format());
        return false;
    }

    private void verifyTeam(Team team) {
        if (team.getPlayers().isEmpty()) {
            this.teamManager.removeTeam(team);
        }
    }

}
