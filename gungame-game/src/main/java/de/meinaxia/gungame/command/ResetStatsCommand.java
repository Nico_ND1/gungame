package de.meinaxia.gungame.command;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.meinaxia.gungame.stats.StatsDatabase;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class ResetStatsCommand implements CommandExecutor {

    private final StatsDatabase statsDatabase;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        if (args.length != 1) {
            player.sendMessage(Messages.RESETSTATS_COMMAND_USAGE.format());
            return false;
        }

        try {
            this.statsDatabase.resetStats(this.fetchUUID(args[0]));
            player.sendMessage(Messages.RESETSTATS_COMMAND_SUCCESS.format());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private UUID fetchUUID(String playerName) throws Exception {
        // Get response from Mojang API
        URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + playerName);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        if (connection.getResponseCode() == 400) {
            System.err.println("There is no player with the name \"" + playerName + "\"!");
            return UUID.randomUUID();
        }

        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        // Parse JSON response and get UUID
        JsonElement element = new JsonParser().parse(bufferedReader);
        JsonObject object = element.getAsJsonObject();
        String uuidAsString = object.get("id").getAsString();

        // Return UUID
        return this.parseUUIDFromString(uuidAsString);
    }

    private UUID parseUUIDFromString(String uuidAsString) {
        String[] parts = {
            "0x" + uuidAsString.substring(0, 8),
            "0x" + uuidAsString.substring(8, 12),
            "0x" + uuidAsString.substring(12, 16),
            "0x" + uuidAsString.substring(16, 20),
            "0x" + uuidAsString.substring(20, 32)
        };

        long mostSigBits = Long.decode(parts[0]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[1]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[2]);

        long leastSigBits = Long.decode(parts[3]);
        leastSigBits <<= 48;
        leastSigBits |= Long.decode(parts[4]);

        return new UUID(mostSigBits, leastSigBits);
    }

}
