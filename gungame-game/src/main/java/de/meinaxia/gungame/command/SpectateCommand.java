package de.meinaxia.gungame.command;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;
import net.sasexception.nicksystem.api.NickAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class SpectateCommand implements CommandExecutor {

    private final GunGame gunGame;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;
        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player);

        gunGamePlayer.setSpectator(!gunGamePlayer.isSpectator());
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (gunGamePlayer.isSpectator()) {
                onlinePlayer.hidePlayer(player);
                onlinePlayer.sendMessage(Messages.QUIT.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
            } else {
                onlinePlayer.showPlayer(player);
                onlinePlayer.sendMessage(Messages.JOIN.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
            }
        }

        player.setGameMode(gunGamePlayer.isSpectator() ? GameMode.SPECTATOR : Bukkit.getDefaultGameMode());
        player.sendMessage(Messages.SPECTATOR_COMMAND_TOGGLE.format(!gunGamePlayer.isSpectator() ? "§ckein " : "§a"));
        if (!gunGamePlayer.isSpectator()) {
            player.teleport(this.gunGame.getMapManager().getCurrentMap().getSpawnLocation());
        }

        return false;
    }
}
