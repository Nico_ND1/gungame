package de.meinaxia.gungame.listener;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.player.GunGamePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class TeamListener implements Listener {

    private final GunGame gunGame;

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) return;
        final Player player = (Player) event.getEntity();
        final Player attacker = (Player) event.getDamager();
        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player);
        final GunGamePlayer gunGameAttacker = this.gunGame.getPlayer(attacker);

        if (!this.gunGame.getTeamManager().canDamage(gunGamePlayer, gunGameAttacker)) {
            event.setCancelled(true);
        }
    }

}
