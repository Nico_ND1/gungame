package de.meinaxia.gungame.listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.CreeperPowerEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityCreatePortalEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.PigZapEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.SheepDyeWoolEvent;
import org.bukkit.event.entity.SheepRegrowWoolEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.spigotmc.event.entity.EntityMountEvent;

/**
 * @author Nico_ND1
 */
public class CancelListener implements Listener {

    @EventHandler
    public void onEvent(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerArmorStandManipulateEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerFishEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerPortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerShearEntityEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerAchievementAwardedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerEditBookEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityExplodeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityTameEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityPortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityMountEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityCreatePortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EntityBreakDoorEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BlockPhysicsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(CraftItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(ItemMergeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BrewEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(FurnaceBurnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(FurnaceSmeltEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PrepareItemEnchantEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(EnchantItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BlockBurnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(BlockDispenseEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(ExplosionPrimeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(SlimeSplitEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(SheepDyeWoolEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(SheepRegrowWoolEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(CreeperPowerEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PigZapEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(PlayerLeashEntityEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEvent(EntityTargetEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEvent(InventoryClickEvent event) {
        event.setCancelled(true);
    }

}
