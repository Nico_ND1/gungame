package de.meinaxia.gungame.listener;
import de.meinaxia.gungame.GunGamePlugin;
import de.meinaxia.gungame.stats.StatisticType;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;
import net.sasexception.nicksystem.api.NickAPI;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class DeathListener implements Listener {

    private final GunGame gunGame;

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        final Player player = event.getPlayer();
        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player.getUniqueId());
        this.gunGame.getLevel(gunGamePlayer.getLevel()).applyItems(player);

        event.setRespawnLocation(this.gunGame.getMapManager().getCurrentMap().getSpawnLocation());
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player.getUniqueId());
        gunGamePlayer.setKillingSpree(0);

        final int level = gunGamePlayer.getLevel() / 2;
        gunGamePlayer.setLevel(level);
        gunGamePlayer.getStatistics().increment(StatisticType.DEATHS);
        this.gunGame.getLevel(level).applyItems(player);
        player.setLevel(level);

        event.setNewLevel(level);
        event.setNewExp(0);
        event.setDroppedExp(0);
        event.setKeepInventory(false);
        event.getDrops().clear();

        Bukkit.getScheduler().runTaskLater(JavaPlugin.getPlugin(GunGamePlugin.class), () -> {
            player.spigot().respawn();
        }, 8);

        if (player.getKiller() != null) {
            final Player killer = player.getKiller();
            final GunGamePlayer killerGamePlayer = this.gunGame.getPlayer(killer.getUniqueId());
            if (killerGamePlayer != null && !killerGamePlayer.isSpectator()) {
                killerGamePlayer.incrementKillingSpree(1);
                if (killerGamePlayer.announceKillingSpree()) {
                    Bukkit.broadcastMessage(Messages.DEATH_KILLING_SPREE.format(NickAPI.getNickRank(killer).getChatColor(), killer.getName(), killerGamePlayer.getKillingSpree()));
                }

                if (killerGamePlayer.getLevel() != this.gunGame.getLevelConfig().getMaxLevels()) {
                    killerGamePlayer.incrementLevel(1);
                    killer.setLevel(killerGamePlayer.getLevel());
                    this.gunGame.getLevel(killerGamePlayer.getLevel()).applyItems(killer);
                    if (!killer.isDead() && killer.getHealth() > 0) killer.setHealth(20);

                    killer.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
                    if (killerGamePlayer.getLevel() > killerGamePlayer.getStatistics().getStat(StatisticType.BEST_LEVEL)) {
                        killerGamePlayer.getStatistics().getDifference().put(StatisticType.BEST_LEVEL, killerGamePlayer.getLevel());
                    }
                }

                killerGamePlayer.getStatistics().increment(StatisticType.KILLS);
                killerGamePlayer.getScoreboard().update(killerGamePlayer);
                event.setDeathMessage(Messages.DEATH_KILLER.format(NickAPI.getNickRank(player).getChatColor(), player.getName(), NickAPI.getNickRank(killer).getChatColor(), killer.getName()));
            }
        } else {
            event.setDeathMessage(Messages.DEATH.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
        }

        gunGamePlayer.getScoreboard().update(gunGamePlayer);
    }

}
