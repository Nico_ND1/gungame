package de.meinaxia.gungame.listener;
import de.meinaxia.gungame.GunGamePlugin;
import de.meinaxia.gungame.stats.StatisticPlayer;
import de.meinaxia.gungame.stats.StatisticType;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.Messages;
import net.meinaxia.gungame.player.GunGamePlayer;
import net.meinaxia.gungame.player.GunGameScoreboard;
import net.sasexception.nicksystem.NickSystem;
import net.sasexception.nicksystem.api.NickAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class ConnectionListener implements Listener {

    private final GunGame gunGame;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final Map<StatisticType, Integer> stats = new HashMap<>();
        for (StatisticType type : StatisticType.values()) {
            stats.put(type, 0);
        }
        this.gunGame.getStatsDatabase().saveStats(new StatisticPlayer(player.getUniqueId(), stats));
        AtomicReference<GunGameScoreboard> reference = new AtomicReference<>();
        if (this.gunGame.useNick()) {
            NickSystem.scoreboards.forEach((player1, scoreboard) -> {
                if (player1.getUniqueId().equals(player.getUniqueId())) {
                    reference.set(new GunGameScoreboard(this.gunGame, scoreboard));
                }

                Objective objective = scoreboard.getObjective(DisplaySlot.BELOW_NAME);
                if (objective == null) {
                    objective = scoreboard.registerNewObjective("showhealth", "health");
                    objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
                    objective.setDisplayName("§c❤");
                }
            });
        } else {
            reference.set(new GunGameScoreboard(this.gunGame, player.getScoreboard() == null ? Bukkit.getScoreboardManager().getNewScoreboard() : player.getScoreboard()));
            player.setScoreboard(reference.get().getScoreboard());
        }
        final GunGamePlayer gunGamePlayer = new GunGamePlayer(player.getUniqueId(), player, null, 0, 0, true, null, reference.get(), false);
        this.gunGame.setPlayer(gunGamePlayer);

        Bukkit.getScheduler().runTaskAsynchronously(JavaPlugin.getPlugin(GunGamePlugin.class), () -> {
            final StatisticPlayer statisticPlayer = this.gunGame.getStatsDatabase().loadPlayer(player.getUniqueId());
            gunGamePlayer.setStatistics(statisticPlayer);
            gunGamePlayer.getScoreboard().update(gunGamePlayer);
        });
        player.teleport(this.gunGame.getMapManager().getCurrentMap().getSpawnLocation());
        this.gunGame.getLevel(0).applyItems(player);

        for (GunGamePlayer gamePlayer : this.gunGame.getPlayers().values()) {
            if (gamePlayer.isSpectator()) {
                player.hidePlayer(gamePlayer.getPlayer());
            }
        }

        event.setJoinMessage(Messages.JOIN.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        final GunGamePlayer gunGamePlayer = this.gunGame.removePlayer(player.getUniqueId());
        if (gunGamePlayer.isSpectator()) {
            event.setQuitMessage(null);
        } else {
            event.setQuitMessage(Messages.QUIT.format(NickAPI.getNickRank(player).getChatColor(), player.getName()));
        }

        Bukkit.getScheduler().runTaskAsynchronously(JavaPlugin.getPlugin(GunGamePlugin.class), () -> {
            this.gunGame.getStatsDatabase().saveStats(gunGamePlayer.getStatistics());
        });

    }

}
