package de.meinaxia.gungame.listener;
import lombok.AllArgsConstructor;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.map.MapManager;
import net.meinaxia.gungame.player.GunGamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class ProtectionListener implements Listener {

    private final GunGame gunGame;
    private final MapManager mapManager;

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        final Player player = event.getPlayer();

        if (player.isDead() || player.getHealth() == 0) return;
        if (event.getTo().getBlock().getType() == Material.WATER || event.getTo().getBlock().getType() == Material.STATIONARY_WATER) {
            final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer(player);
            if (!gunGamePlayer.isSpectator()) player.setHealth(0);
        }
    }

    @EventHandler
    public void onEvent(EntityDamageEvent event) {
        if (event.getCause() == DamageCause.FALL || this.contains(event.getEntity().getLocation())) {
            event.setCancelled(true);
        } else {
            if (event.getEntity() instanceof Player) {
                final GunGamePlayer gunGamePlayer = this.gunGame.getPlayer((Player) event.getEntity());
                if (gunGamePlayer.isSpectator()) event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEvent(EntityDamageByEntityEvent event) {
        if (this.contains(event.getDamager().getLocation())) {
            event.setCancelled(true);
        }
    }

    private boolean contains(Location location) {
        return this.mapManager.getCurrentMap().getCuboid().contains(location);
    }

}
