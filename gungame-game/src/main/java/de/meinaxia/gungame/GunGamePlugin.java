package de.meinaxia.gungame;
import de.meinaxia.gungame.command.ResetStatsCommand;
import de.meinaxia.gungame.command.SetLevelCommand;
import de.meinaxia.gungame.command.SpectateCommand;
import de.meinaxia.gungame.command.StatsCommand;
import de.meinaxia.gungame.command.TeamCommand;
import de.meinaxia.gungame.command.TeleportCommand;
import de.meinaxia.gungame.listener.CancelListener;
import de.meinaxia.gungame.listener.ConnectionListener;
import de.meinaxia.gungame.listener.DeathListener;
import de.meinaxia.gungame.listener.ProtectionListener;
import de.meinaxia.gungame.listener.TeamListener;
import de.meinaxia.gungame.stats.StatsConfig;
import de.meinaxia.gungame.stats.mysql.StatsDatabaseMongoDB;
import net.meinaxia.gungame.GunGame;
import net.meinaxia.gungame.level.LevelConfig;
import net.meinaxia.gungame.map.MapConfig;
import net.meinaxia.gungame.map.manager.DefaultMapManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.FileNotFoundException;

/**
 * @author Nico_ND1
 */
public class GunGamePlugin extends JavaPlugin {

    private GunGame gunGame;

    @Override
    public void onEnable() {
        try {
            final DefaultMapManager mapManager = new DefaultMapManager(MapConfig.loadConfig("plugins/GunGame/maps.json"));
            this.gunGame = new GunGame(
                LevelConfig.loadConfig("plugins/GunGame/levels.json"),
                mapManager,
                new StatsDatabaseMongoDB(StatsConfig.loadConfig("plugins/GunGame/database.json"))
            );
            mapManager.setGunGame(this.gunGame);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bukkit.getPluginManager().registerEvents(new ConnectionListener(this.gunGame), this);
        Bukkit.getPluginManager().registerEvents(new DeathListener(this.gunGame), this);
        Bukkit.getPluginManager().registerEvents(new CancelListener(), this);
        Bukkit.getPluginManager().registerEvents(new ProtectionListener(this.gunGame, this.gunGame.getMapManager()), this);
        Bukkit.getPluginManager().registerEvents(new TeamListener(this.gunGame), this);

        this.getCommand("team").setExecutor(new TeamCommand(this.gunGame, this.gunGame.getTeamManager(), this.gunGame.getMapManager()));
        this.getCommand("stats").setExecutor(new StatsCommand(this.gunGame));
        this.getCommand("spectate").setExecutor(new SpectateCommand(this.gunGame));
        this.getCommand("teleport").setExecutor(new TeleportCommand(this.gunGame));
        this.getCommand("setlevel").setExecutor(new SetLevelCommand(this.gunGame));
        this.getCommand("resetstats").setExecutor(new ResetStatsCommand(this.gunGame.getStatsDatabase()));
    }
}
